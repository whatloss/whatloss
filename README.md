# whatloss

`whatloss` transcodes audio files and creates torrents for them. Decoders,
encoders and the tooling for creating the torrents can be configured, which
means that `whatloss` does not make any assumptions on what other tools you
might want to use.

A sample config for usage with `flac`, `metaflac`, `jq`, `lame` and `imdl` is
provided, but you could also have `wav` files, with tags in adjacent `json`
files, encoding with `opusenc` and creating torrents with `mktorrent` if you set
up the config file accordingly.

The commands that `whatloss` is running are templated using
[Tera](https://tera.netlify.app/docs/#introduction), a templating engine for
Rust inspired by Jinja 2 and Django templates. 

## Platform Compatibility

`whatloss` aims to be compatible with all major platforms - in the long run. For
now, it's being developed and tested primarily on Linux. It might also work on
other Unix-like OSs, such as MacOS, but it will definitely not work on Windows
right now. Support for that is planned though!

## Installation

Use the package manager [cargo](https://doc.rust-lang.org/cargo/index.html) to
install whatloss.

```bash
cargo install --git https://codeberg.org/whatloss/whatloss.git --branch main
```

## Usage

After installing `whatloss`, you have to configure it. The config file is
expected to be in the default location for your specific platform. For Linux,
that means `${XDG_CONFIG_DIR}/whatloss/config.yaml`, for Windows it's
`C:\Users\Alice\AppData\Roaming\whatloss\whatloss\config.yaml`, and for MacOS
it's `/Users/Alice/Library/Application
Support/org.codeberg.whatloss.whatloss/config.yaml`. For most users, the sample
config should provide everything needed audio wise, with the only thing remaing
to be done being configuring your trackers.

Now that `whatloss` is installed and configured, you can run it:

```text
whatloss 0.1.0
Steven Letterpress <stevenletterpress@420blaze.it>
highly configurable music torrent preparation tool

whatloss is a music torrent preparation tool which takes in a folder of lossless
music, transcodes it with lossy encoders according to the configured presets and then
creates torrent files for each of them.

USAGE:
    whatloss [OPTIONS] <FOLDER>

ARGS:
    <FOLDER>
            Folder that contains the music you want to transcode. This folder name
            must include the name of the decoder that should be used for this folder,
            for both detection of the right decoder and for placing the encoding
            preset name in the output folders

OPTIONS:
    -c, --config <CONFIG>
            Path to your config file. Defaults to the usual place for your platform.
            If whatloss can't find your config file, the path will be printed in the
            error message

    -h, --help
            Print help information

    -V, --version
            Print version information
```

As you can see from this `whatloss --help` output, it expects you to run
`whatless <FOLDER>`, where `FOLDER` is the placeholder for the folder you want
to transcode. `FOLDER` needs to contain the name of the decoder you intend to be
using (so usually `FLAC`), so that whatloss can both detect the decoder using it
and know where to place the name of the encoding preset afterwards. This should
be enough to get you started.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to
discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[CNPL-7.0-or-later](https://thufie.lain.haus/NPL.html)
