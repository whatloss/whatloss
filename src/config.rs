use std::{collections::HashMap, fs::File, path::PathBuf};

use directories::ProjectDirs;
use serde::Deserialize;
use thiserror::Error;

use crate::{
    transcode::{Decoder, Encoder, TorrentCreator},
    PresetOptions, Tracker,
};

#[derive(Deserialize)]
pub struct EncoderConfig {
    pub command: String,
    pub extension: String,
}

#[derive(Deserialize)]
pub struct DecoderConfig {
    pub extensions: Vec<String>,
    pub command: String,
    pub tag_sniffer: String,
}

#[derive(Deserialize)]
pub struct PresetConfig {
    pub encoder: String,
    #[serde(flatten)]
    pub variables: PresetOptions,
}

#[derive(Deserialize)]
pub struct ConfigFile {
    #[serde(default = "default_transfer")]
    pub transfer: Vec<String>,
    pub decoders: HashMap<String, DecoderConfig>,
    pub encoders: HashMap<String, EncoderConfig>,
    pub presets: HashMap<String, PresetConfig>,
    pub torrent_creator: String,
    pub trackers: Vec<Tracker>,
}

fn default_transfer() -> Vec<String> {
    Vec::from(["jpg".into(), "png".into()])
}

pub struct Config {
    pub transfer: Vec<String>,
    pub decoders: Vec<Decoder>,
    pub encoders: Vec<Encoder>,
    pub torrent_creator: TorrentCreator,
    pub trackers: Vec<Tracker>,
}

impl TryFrom<ConfigFile> for Config {
    type Error = ConfigError;

    fn try_from(cf: ConfigFile) -> Result<Self, Self::Error> {
        let transfer = cf.transfer;
        let decoders = cf
            .decoders
            .iter()
            .map(|(name, decoder)| (name, decoder).try_into())
            .collect::<Result<Vec<Decoder>, Self::Error>>()?;
        let encoders = cf
            .presets
            .iter()
            .map(|(name, preset)| {
                let encoder = match cf.encoders.get(&preset.encoder) {
                    Some(encoder) => encoder,
                    None => {
                        return Err(ConfigError::PresetEncoderMismatch {
                            preset: name.to_string(),
                            encoder: preset.encoder.clone(),
                        })
                    }
                };
                (encoder, preset, name).try_into()
            })
            .collect::<Result<Vec<Encoder>, Self::Error>>()?;
        let torrent_creator = cf.torrent_creator.try_into()?;
        let trackers = cf.trackers;

        Ok(Self {
            transfer,
            decoders,
            encoders,
            torrent_creator,
            trackers,
        })
    }
}

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error(
        "Preset {preset} references encoder {encoder}, which was not found in your config file."
    )]
    PresetEncoderMismatch { preset: String, encoder: String },
    #[error("Tera failed to compile template: {0}")]
    Tera(#[from] tera::Error),
    #[error("Failed to deserialize config: {0}")]
    Serde(#[from] serde_yaml::Error),
    #[error("Failed to read config file: {0}")]
    Io(#[from] std::io::Error),
    #[error("Couldn't resolve the project directory")]
    ProjectDir,
}

pub fn load_config(config_path: Option<String>) -> Result<Config, ConfigError> {
    let config_path: PathBuf = if let Some(config_path) = config_path {
        PathBuf::from(config_path)
    } else {
        let project_dirs = ProjectDirs::from("org.codeberg", "whatloss", "whatloss")
            .ok_or(ConfigError::ProjectDir)?;
        let mut config_path = project_dirs.config_dir().to_path_buf();
        config_path.push("config.yaml");
        config_path
    };

    let config_file = File::open(&config_path)?;
    let config: ConfigFile = serde_yaml::from_reader(config_file)?;
    config.try_into()
}
