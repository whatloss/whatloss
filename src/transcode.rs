use std::{
    borrow::Cow,
    collections::HashMap,
    ffi::OsString,
    io::Read,
    path::{Path, PathBuf},
    process::{Command, Stdio},
};

use anyhow::Context;
use osstrtools::OsStrTools;
use tera::Tera;
use thiserror::Error;

use crate::{
    config::{ConfigError, DecoderConfig, EncoderConfig, PresetConfig},
    Tags, Tracker,
};

pub struct ShellEscape {}

impl tera::Filter for ShellEscape {
    fn filter(
        &self,
        value: &serde_json::Value,
        _args: &HashMap<String, serde_json::Value>,
    ) -> tera::Result<serde_json::Value> {
        Ok(serde_json::Value::String(match value {
            serde_json::Value::String(string) => shell_escape::escape(Cow::Borrowed(string)).into(),
            _ => todo!(),
        }))
    }
}

pub struct TorrentCreator {
    tera: Tera,
}

impl TryFrom<String> for TorrentCreator {
    type Error = ConfigError;

    fn try_from(template: String) -> Result<Self, Self::Error> {
        let mut tera = Tera::default();
        tera.register_filter("shell_escape", ShellEscape {});
        tera.add_raw_template("torrent_creator", &template)?;
        Ok(Self { tera })
    }
}

impl TorrentCreator {
    pub fn create(&self, tracker: &Tracker, folder: &Path) -> anyhow::Result<()> {
        let mut context = tera::Context::new();
        for (key, value) in tracker {
            context.insert(key, value);
        }
        context.insert(
            "folder",
            folder
                .to_str()
                .context(file_string_error("folder", folder))?,
        );
        Command::new("bash")
            .args(["-c", &self.tera.render("torrent_creator", &context)?])
            .output()
            .context(format!(
                "Failed to create torrent for {}",
                folder.to_string_lossy()
            ))?;
        Ok(())
    }
}

pub struct Encoder {
    name: String,
    tera: Tera,
    context: tera::Context,
    extension: String,
}

impl TryFrom<(&EncoderConfig, &PresetConfig, &String)> for Encoder {
    type Error = ConfigError;

    fn try_from(
        (encoder_config, preset_config, preset_name): (&EncoderConfig, &PresetConfig, &String),
    ) -> Result<Self, Self::Error> {
        let mut tera = Tera::default();
        tera.register_filter("shell_escape", ShellEscape {});
        let mut context = tera::Context::default();
        let name = preset_name.to_owned();
        let extension = encoder_config.extension.clone();
        tera.add_raw_template("encoder", &encoder_config.command)?;
        for (key, value) in &preset_config.variables {
            context.insert(key, &value);
        }
        Ok(Self {
            name,
            tera,
            context,
            extension,
        })
    }
}

impl Encoder {
    pub fn encode(&self, file: &Path, tags: &Tags) -> anyhow::Result<Command> {
        let mut context = self.context.clone();
        context.insert(
            "file",
            file.to_str().context(file_string_error("file", file))?,
        );
        context.insert("tags", tags);
        let mut command = Command::new("bash");
        command
            .args(["-c", &self.tera.render("encoder", &context)?])
            .stdout(Stdio::piped())
            .stderr(Stdio::piped());
        Ok(command)
    }
}

pub struct Decoder {
    pub name: String,
    tera: Tera,
    pub extensions: Vec<String>,
}

impl TryFrom<(&String, &DecoderConfig)> for Decoder {
    type Error = ConfigError;

    fn try_from((name, decoder_config): (&String, &DecoderConfig)) -> Result<Self, Self::Error> {
        let name = name.to_owned();
        let mut tera = Tera::default();
        tera.register_filter("shell_escape", ShellEscape {});
        tera.add_raw_template("decoder", &decoder_config.command)?;
        tera.add_raw_template("sniffer", &decoder_config.tag_sniffer)?;
        let extensions = decoder_config.extensions.clone();
        Ok(Self {
            name,
            tera,
            extensions,
        })
    }
}

impl Decoder {
    pub fn sniff(&self, file: &Path) -> anyhow::Result<Tags> {
        let mut context = tera::Context::new();
        context.insert(
            "file",
            file.to_str().context(file_string_error("file", file))?,
        );
        let sniffed_tags = Command::new("bash")
            .args(["-c", &self.tera.render("sniffer", &context)?])
            .output()
            .context(format!(
                "Failed to sniff tags from {}",
                file.to_string_lossy()
            ))?;
        let sniffed_tags: Tags = serde_json::from_slice(&sniffed_tags.stdout).context(format!(
            "Failed to parse sniffed tags from {}",
            file.to_string_lossy()
        ))?;
        Ok(sniffed_tags)
    }

    pub fn decode(&self, file: &Path) -> anyhow::Result<Command> {
        let mut context = tera::Context::new();
        context.insert(
            "file",
            file.to_str().context(file_string_error("file", file))?,
        );
        let mut command = Command::new("bash");
        command
            .args(["-c", &self.tera.render("decoder", &context)?])
            .stdout(Stdio::piped())
            .stderr(Stdio::piped());
        Ok(command)
    }

    pub fn matches(&self, folder: &Path) -> bool {
        if let Some(file_name) = folder.file_name() {
            file_name.contains(&*self.name)
        } else {
            false
        }
    }
}

pub struct Prepared {
    decoder: Command,
    encoder: Command,
}

impl Prepared {
    pub fn transcode(
        decoder: &Decoder,
        encoders: &[Encoder],
        folder: &Path,
        transfer: Vec<String>,
    ) -> anyhow::Result<(Vec<Self>, Vec<PathBuf>)> {
        let files_to_copy = list_files(&transfer, folder)?;
        let files_to_transcode = list_files(&decoder.extensions, folder)?;

        println!("[1/4] Sniffing tags");
        let pb = indicatif::ProgressBar::new(files_to_transcode.len() as u64);

        let files_to_transcode = files_to_transcode
            .into_iter()
            .map(|file| {
                let ret_val = decoder.sniff(&file).map(|tags| (file, tags));
                pb.inc(1);
                ret_val
            })
            .collect::<anyhow::Result<Vec<(PathBuf, Tags)>>>()?;
        pb.finish_and_clear();

        let mut transcodes = Vec::new();
        let mut torrent_targets = vec![folder.to_owned()];

        println!("[2/4] Copying non-audio files");
        let pb = indicatif::ProgressBar::new((files_to_copy.len() * encoders.len()) as u64);

        for encoder in encoders {
            let target_folder = PathBuf::from(
                folder
                    .file_name()
                    .context(no_file_name("folder", folder))?
                    .replace(&*decoder.name, &*encoder.name),
            );
            torrent_targets.push(target_folder.to_owned());

            std::fs::create_dir_all(&target_folder)?;

            for file in &files_to_copy {
                let file_name = file.file_name().context(no_file_name("file", file))?;
                let mut target = target_folder.clone();
                target.push(file_name);
                std::fs::copy(file, target)?;
                pb.inc(1);
            }

            for (file, tags) in &files_to_transcode {
                let file_name = file.file_name().context(no_file_name("file", file))?;
                let mut target = target_folder.clone();
                target.push(file_name);
                target.set_extension(&encoder.extension);
                let decoder = decoder.decode(file)?;
                let encoder = encoder.encode(&target, tags)?;
                transcodes.push(Self { decoder, encoder })
            }
        }
        pb.finish_and_clear();

        Ok((transcodes, torrent_targets))
    }

    pub fn run(mut self) -> anyhow::Result<()> {
        let mut decoder = self.decoder.spawn()?;
        self.encoder.stdin(decoder.stdout.take().context(
            "failed to access the decoder's stdout stream for handing over to \
            the encoder",
        )?);
        let encoder = self.encoder.output()?;
        let mut decoder_stderr = decoder.stderr.take().context(
            "failed to access the decoder's stderr stream for capturing \
            potential errors",
        )?;
        let mut decoder_stderr_buf = Vec::new();
        let decoder = decoder.wait()?;
        decoder_stderr.read_to_end(&mut decoder_stderr_buf)?;

        decoder
            .success()
            .then(|| ())
            .ok_or_else(|| TranscodeError::Decode(decoder_stderr_buf.into()))?;
        encoder
            .status
            .success()
            .then(|| ())
            .ok_or_else(|| TranscodeError::Encode(encoder.stderr.into()))?;

        Ok(())
    }
}

fn list_files(extensions: &[String], folder: &Path) -> anyhow::Result<Vec<PathBuf>> {
    let extensions: Vec<OsString> = extensions.iter().map(OsString::from).collect();
    let paths = folder
        .read_dir()
        .context("TODO")?
        .filter_map(|entry| Some(entry.ok()?.path()))
        .filter(|entry| entry.is_file())
        .filter(|entry| {
            entry
                .extension()
                .filter(|extension| extensions.contains(&extension.to_os_string()))
                .is_some()
        })
        .collect();
    Ok(paths)
}

fn file_string_error(kind: &str, name: &Path) -> String {
    format!(
        "Couldn't turn name of {} {} into a String, due to non-unicode \
        characters. Please only use valid UTF-8 characters in file and \
        directory names.",
        kind,
        name.to_string_lossy()
    )
}

fn no_file_name(kind: &str, name: &Path) -> String {
    format!(
        "Failed to determine {} name for path {}",
        kind,
        name.to_string_lossy()
    )
}

#[derive(Error, Debug)]
pub enum TranscodeError {
    #[error("Failed to decode, stderr output: {0}")]
    Decode(Log),
    #[error("Failed to encode, stderr output: {0}")]
    Encode(Log),
}

#[derive(Debug)]
pub enum Log {
    Unicode(String),
    Binary(Vec<u8>),
}

impl std::fmt::Display for Log {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Log::Unicode(log) => f.write_str(log),
            Log::Binary(bytes) => {
                f.write_str(
                    "log was not decodable as unicode, binary output follows hex encoded: ",
                )?;
                for byte in bytes {
                    f.write_fmt(format_args!("{:x}", byte))?;
                }
                Ok(())
            }
        }
    }
}

impl From<Vec<u8>> for Log {
    fn from(bytes: Vec<u8>) -> Self {
        match String::from_utf8(bytes) {
            Ok(string) => Self::Unicode(string),
            Err(utf8err) => Self::Binary(utf8err.into_bytes()),
        }
    }
}
