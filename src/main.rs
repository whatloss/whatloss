use std::{collections::HashMap, path::PathBuf};

use anyhow::{Context, Result};
use clap::Parser;
use cli::Cli;
use itertools::Itertools;
use rayon::prelude::*;
use serde_yaml::Value;
use transcode::Prepared;

mod cli;
mod config;
mod transcode;

type Tags = HashMap<String, String>;
type Tracker = HashMap<String, Value>;
type PresetOptions = HashMap<String, Value>;

fn main() -> Result<()> {
    let cli = Cli::parse();
    let config = config::load_config(cli.config)?;

    let folder = cli.folder;

    let decoder = config
        .decoders
        .iter()
        .find(|decoder| decoder.matches(&folder))
        .context(format!(
            "Couldn't find a matching decoder out of {} for folder name {}. \
            The folder name must to include the name of the decoder to use.",
            config
                .decoders
                .iter()
                .map(|decoder| &decoder.name)
                .join(","),
            folder.to_string_lossy()
        ))?;

    let (transcodes, torrent_targets) =
        Prepared::transcode(decoder, &config.encoders, &folder, config.transfer)?;

    println!("[3/4] Transcoding music");
    let pb = indicatif::ProgressBar::new(transcodes.len() as u64);
    pb.set_position(0);

    transcodes
        .into_par_iter()
        .map(|prep| {
            prep.run()?;
            pb.inc(1);
            Ok(())
        })
        .collect::<anyhow::Result<()>>()?;

    pb.finish_and_clear();

    let target_tracker_combos: Vec<(&Tracker, PathBuf)> = config
        .trackers
        .iter()
        .cartesian_product(torrent_targets)
        .collect();

    println!("[4/4] Creating torrent files");
    let pb = indicatif::ProgressBar::new(target_tracker_combos.len() as u64);

    target_tracker_combos
        .par_iter()
        .map(|(tracker, target)| {
            config.torrent_creator.create(tracker, target)?;
            pb.inc(1);
            Ok(())
        })
        .collect::<anyhow::Result<()>>()?;

    pb.finish_and_clear();

    Ok(())
}
