use std::path::PathBuf;

use clap::Parser;

#[derive(Parser)]
#[clap(author, version)]
/// highly configurable music torrent preparation tool
///
/// whatloss is a music torrent preparation tool which takes in a folder of
/// lossless music, transcodes it with lossy encoders according to the
/// configured presets and then creates torrent files for each of them.

pub struct Cli {
    /// Folder that contains the music you want to transcode. This folder name
    /// must include the name of the decoder that should be used for this
    /// folder, for both detection of the right decoder and for placing the
    /// encoding preset name in the output folders.
    pub folder: PathBuf,
    /// Path to your config file. Defaults to the usual place for your platform.
    /// If whatloss can't find your config file, the path will be printed in the
    /// error message.
    #[clap(short, long)]
    pub config: Option<String>,
}
